#![feature(coerce_unsized, unsize, drop_in_place)]

use std::mem;
use std::ptr;
use std::ops::{ Deref, DerefMut, CoerceUnsized };
use std::marker::{ PhantomData, Unsize };

pub struct Arena {
    mem: Vec<u8>,
}

impl Arena {
    pub fn with_capacity(c: usize) -> Arena {
        Arena {
            mem: Vec::with_capacity(c)
        }
    }

    pub fn begin_allocation(&mut self) -> Allocator {
        self.mem.clear();
        Allocator {
            mem: &mut self.mem
        }
    }
}

pub struct Allocator<'a> {
    mem: &'a mut Vec<u8>
}

impl<'a> Allocator<'a> {
    pub fn allocate<'b, T: 'b>(&mut self, value: T) -> Allocated<'a, 'b, T> {
        let offset = self.mem.len();
        let align = mem::align_of::<T>();
        let m = offset % align;
        let aligned_offset = if m == 0 { offset } else { offset + align - m };

        let new_len = aligned_offset + mem::size_of::<T>();

        if new_len <= self.mem.capacity() {
            unsafe {
                self.mem.set_len(new_len); 

                let ptr = &mut self.mem.get_unchecked_mut(aligned_offset) as *mut _ as *mut T;
                ptr::write(ptr, value);

                Allocated {
                    reference: &mut *ptr,
                    phantom: PhantomData
                }
            }
        } else {
            panic!("Arena overflow");
        }
    }
}

pub struct Allocated<'a, 'b, T: 'b + ?Sized> {
    reference: &'b mut T,
    phantom: PhantomData<&'a ()>
}

impl<'a, 'b, T: 'b + ?Sized> Deref for Allocated<'a, 'b, T> {
    type Target = T;

    fn deref(&self) -> &T {
        &*self.reference
    }
}

impl<'a, 'b, T: 'b + ?Sized> DerefMut for Allocated<'a, 'b, T> {
    fn deref_mut(&mut self) -> &mut T {
        self.reference
    }
}

impl<'a, 'b, T: 'b + ?Sized> Drop for Allocated<'a, 'b, T> {
    fn drop(&mut self) {
        unsafe {
            ptr::drop_in_place(self.reference);
        }
    }
}

impl<'a, 'b, T: ?Sized, U: ?Sized> CoerceUnsized<Allocated<'a, 'b, U>> for Allocated<'a, 'b, T>
    where U: 'b, T: 'b + Unsize<U> {}

#[cfg(test)]
mod test {
    use super::*;
    use std::mem;

    #[test]
    fn simple() {
        let mut arena = Arena::with_capacity(mem::size_of::<u32>() * 10);
        for _ in 0..10 {
            let mut allocator = arena.begin_allocation();
            for i in 0..10u32 {
                let allocated = allocator.allocate(i);
                assert!(&allocated as &u32 == &i);
            }
        }
    }

    struct Zeroer<'a> {
        number: &'a mut u32
    }

    impl<'a> Drop for Zeroer<'a> {
        fn drop(&mut self) {
            *self.number = 0;
        }
    }

    #[test]
    fn drop() {
        let mut arena = Arena::with_capacity(mem::size_of::<Zeroer>());
        let mut allocator = arena.begin_allocation();
        let mut x = 1;
        {
            let _zeroer = allocator.allocate(Zeroer { number: &mut x });
        }
        assert!(x == 0);
    }

    trait Number {
        fn get(&self) -> u32;
    }

    impl<'a> Number for Zeroer<'a> {
        fn get(&self) -> u32 {
            *self.number
        }
    }

    #[test]
    fn coerce_unsized() {
        let mut arena = Arena::with_capacity(mem::size_of::<Zeroer>());
        let mut allocator = arena.begin_allocation();
        let x = 1;
        let mut y = x;
        let zeroer = allocator.allocate(Zeroer { number: &mut y });
        let number = zeroer as Allocated<Number>;
        assert!(number.get() == x);
    }
}
